Source: libio-tee-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Florian Schlichting <fsfs@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: perl
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libio-tee-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libio-tee-perl.git
Homepage: https://metacpan.org/release/IO-Tee
Rules-Requires-Root: no

Package: libio-tee-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends}
Multi-Arch: foreign
Description: module for multiplexing output to multiple output handles
 IO::Tee objects can be used to multiplex input and output in two
 different ways.  The first way is to multiplex output to zero or more
 output handles.  The IO::Tee constructor, given a list of output
 handles, returns a tied handle that can be written to.  When written
 to (using print or printf), the IO::Tee object multiplexes the
 output to the list of handles originally passed to the constructor.
 As a shortcut, you can also directly pass a string or an array
 reference to the constructor, in which case IO::File::new is called
 for you with the specified argument or arguments.
 .
 The second way is to multiplex input from one input handle to zero or
 more output handles as it is being read.  The IO::Tee constructor,
 given an input handle followed by a list of output handles, returns a
 tied handle that can be read from as well as written to.  When written
 to, the IO::Tee object multiplexes the output to all handles passed
 to the constructor, as described in the previous paragraph.  When read
 from, the IO::Tee object reads from the input handle given as the
 first argument to the IO::Tee constructor, then writes any data
 read to the output handles given as the remaining arguments to the
 constructor.
